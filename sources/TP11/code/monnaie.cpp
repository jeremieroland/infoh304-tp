#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#define INFINITY 10000
using namespace std;

// Tableau de valeurs de pièces
vector<int> valeurs;
// Nombre de valeurs de pièces
int n;

int ntot(int m)
{
	// A compléter
    return 0;
}

int main(int argc, const char *argv[])
{
    // Renvoie le nombre de pièces nécessaires pour rendre le montant m passé en paramètre
    if (argc <= 2) {
        cout << "Précisez en paramètres le montant à rendre suivi des valeurs de pièces disponibles." << endl;
        return 1;
    }
    else {
        // On transforme les paramètres passés sur la ligne de commande en entiers (montant et valeurs des pièces)
        int m = atoi(argv[1]);
        n = argc-2;
        for (int i=2; i<argc; i++) {
			valeurs.push_back(atoi(argv[i]));
			if (valeurs[i-2] == 0) {
				cout << "Les valeurs de pièces doivent être des entiers positifs." << endl;
				return 1;
			}
		}
		// Tri des valeurs de pièces par ordre croissant
		sort(valeurs.begin(), valeurs.end());
        int npieces = ntot(m);
        if (npieces >= INFINITY) {
            cout << "Pas moyen de rendre un montant " << m << "." << endl;
        }
        else {
            cout << "Pour rendre le montant " << m << ", il faut " << npieces << " pièces." << endl;
        }
    }
    return 0;
}
