class Entier {
private:
	int val;
public:
	Entier( int valInit = 0) : val( valInit ) {}

	~Entier()
		{} // Ne fait rien car le seul attribut est de type primitif

	Entier( const Entier & EntierOriginal ) : val( EntierOriginal.val )
		{} // Construit un nouvel Entier initialisé avec l'original

	const Entier & operator= ( const Entier & EntierOriginal)
	{
		if ( this != &EntierOriginal ) // Vérifie qu'on ne copie pas l'objet sur lui-même
			val = EntierOriginal.val;
		return *this;
	}
};
