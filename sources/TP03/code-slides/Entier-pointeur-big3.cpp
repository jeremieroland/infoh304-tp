class Entier2 {
private:
	int* val;
public:
	Entier2( int valInit = 0)
		{ val = new int(valInit); }

	~Entier2()
		{ delete val; }

	Entier2( const Entier2 & EntierOriginal )
		{ val = new int (*EntierOriginal.val); }

	const Entier2 & operator= ( const Entier2 & EntierOriginal)
	{
		if ( this != &EntierOriginal )
			*val = *EntierOriginal.val;
		return *this;
	}

};
