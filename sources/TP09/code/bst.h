#include <vector>
#include <iostream>
#include <cmath>
#include "node.h"

using namespace std;

class BinarySearchTree
{
private:
	Node* root;
	Node* tree_search(int content);
	Node* tree_search(int content, Node* location);
	void delete_no_child(Node* location);
	void delete_left_child(Node* location);
	void delete_right_child(Node* location);
	void delete_two_children(Node* location);
	
public:
	BinarySearchTree() : root(NULL) {}
	bool isEmpty();
	void print_tree();
	pair<int,int> create_tree_lines(Node* start, vector<string>& value_lines, vector<string>& split_lines, vector<string>& drop_lines, int depth = 0, int previous_width = 0);
	void inorder(Node* location);
	void print_inorder();
	void insert_element(int content);
	void delete_element(int content);
};
