#include <string>
#include "node.h"

Node* Node::getLeft() const
{
	return left;
}
void Node::setLeft(Node* n)
{
	left=n;
}
Node* Node::getRight() const
{
	return right;
}
void Node::setRight(Node* n)
{
	right=n;
}
Node* Node::getParent() const
{
	return parent;
}
void Node::setParent(Node* n)
{
	parent=n;
}
int Node::getContent() const
{
	return content;
}
void Node::setContent(int newcontent)
{
	content=newcontent;
}

