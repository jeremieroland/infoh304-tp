class Node
{
private:
	Node* left;
	Node* right;
	Node* parent;
	int content;
	
public:
	Node(int contentinit=0) : content(contentinit), left(NULL), right(NULL), parent(NULL) {}
	Node* getLeft() const;
	void setLeft(Node* n);
	Node* getRight() const;
	void setRight(Node* n);
	Node* getParent() const;
	void setParent(Node* n);
	int getContent() const;
	void setContent(int newcontent);
};
