\documentclass[a4paper,11pt]{article}

\newcommand{\thetitle}{
Séance 10
}

\input{../common/preamble-TP}

\section*{Réalisation d'une fonction de hachage}
Les fonctions de hachage jouent un rôle très important dans l'architecture informatique moderne. Ces fonctions sont omniprésentes: dans les systèmes de sécurité, de stockage et même dans tous les \textit{objets} d'un programme écrit dans un langage orienté objet (la fonction \lstinline{toString()} de Java renvoie l'empreinte (\textit{hash}) de chaque objet par défaut). Le but de cette séance est de réaliser une fonction de hachage simple qui convertit une string en une paire d'entiers compris entre $1$ et $128$. Une bonne fonction de hachage doit être
\begin{itemize}
\item Déterministe. % Chaque string doit être associée à une seule paire d'entiers.
Chaque appel à la fonction de hachage avec la même string doit avoir la même paire comme valeur de retour.
\item Uniforme. Les paires d'entiers doivent avoir l'apparence d'une distribution aléatoire. Par exemple, pour deux mots ``proches'' comme \textit{see} et \textit{sea}, l'un pourrait être associé à $\{10,28\}$, et l'autre à $\{128,6\}$.
\end{itemize}

Vous trouverez le programme \lstinline{hash.cpp} et un Makefile pour le compiler sur l'université virtuelle. Ce programme permet de visualiser les résultats de la fonction de hachage comme des points sur l'écran. Pour le moment, la fonction \lstinline{hash_function()} ignore l'entrée et donne une paire d'entiers aléatoires à chaque appel. C'est à vous de réaliser une vraie fonction de hachage.

% Note: le dernier exercice vous permettra de comparer votre fonction de hachage avec celle de la bibliothèque \lstinline{boost}. Cette bibliothèque est disponible sur les machines de la salle informatique, mais pour l'installer sur la machine virtuelle, vous devez entrer la commande suivante:
% \begin{lstlisting}
% sudo apt-get install libboost1.54-dev
% \end{lstlisting}
% Pour rappel, le mot de passe est "ulb".

\begin{exercice}
Compilez le programme \lstinline{hash.cpp} avec \lstinline{make} et lancez le programme avec un fichier de texte donné en paramètre (par exemple ``Sherlock Holmes'').
\end{exercice}

Quand vous lancez le programme avec un fichier de texte assez long, les points rouges sur l'écran se dispersent aléatoirement parce que la fonction \lstinline{hash_function()} dans le programme original n'est pas une vraie fonction de hachage mais juste une fonction aléatoire. Idéalement, votre fonction de hachage devrait avoir une apparence similaire sur l'écran mais d'une façon déterministe. Voici quelques élements qui pourraient vous aider:
\begin{itemize}
	\item Vous pouvez accéder directement aux caractères dans la string \lstinline{word} avec la syntaxe \lstinline{word[i]} comme pour un tableau. Comme chaque caractère imprimable est codé par un entier entre 0 et 127 (codes ASCII), vous pouvez directement traiter les caractères \lstinline{word[i]} comme des nombres entiers.
	\item Un caractère (type \code{char}) étant codé sur 1 octet (8 bits) en C/C++, ce type de variable n'est pas suffisant pour générer les grands nombres que vous allez rencontrer pour traiter de longs mots. Dès lors, il est recommandé d'utiliser le type \code{unsigned long long} qui est codé sur 8 octets (64 bits) et permet donc de coder des entiers jusque $2^{64}-1$. Plutôt que d'effectuer vos calculer directement sur le caractère \code{word[i]}, vous pouvez le convertir en utilisant un ``type cast'' via la syntaxe \code{(unsigned long long)word[i]} ou en copiant la valeur de \code{word[i]} dans une variable intermédiaire de type \code{unsigned long long}, ce qui forcera un tel ``type cast''.
	\item Le calcul d'une puissance $x^y$ se fait via la fonction \code{pow(x,y)} (attention, l'opérateur ``\code{^}'' est le ``ou exclusif'' XOR et ne calcule donc pas une puissance), cette fonction étant fournie, notamment, par le fichier header \code{<random>}. La valeur de retour de la fonction \code{pow()} étant de type \code{double} (réel à virgule flottante de précision double), pour éviter des erreurs lors de la manipulation de grands nombres de type \code{unsigned long long}, il faut également effectuer un ``type cast'' de la valeur de retour de cette fonction, via la syntaxe \code{(unsigned long long)pow(x,y)}.
	\item L'opérateur modulo s'écrit \lstinline{%} en C/C++.
	\item Les opérateurs de shift \code{<<} et \code{>>} permettent de décaler une chaîne de bits \code{x} de \code{s} positions vers la gauche ou vers la droite via les syntaxes \code{x << s} et \code{x >> s}. Pour rappel, n'importe quel entier peut-être vu comme une chaine de bits (représentation binaire), et lors d'un décalage vers la droite les bits ``derrière la virgule'' seront tronqués (idem pour un décalage vers la gauche si l'opération mène à un entier plus grand que ce qui peut être codé par le type de variable utilisé).
\end{itemize}

\begin{exercice}
Avant de réaliser le fonction de hachage proprement dite, créez une fonction de préhachage qui prend un mot (type \lstinline{string}) en entrée et renvoie un entier calculé à partir des lettres du mot (pour pouvoir traiter des mots suffisamment longs, utilisez le type \lstinline{unsigned long long} pour la valeur de retour).
\end{exercice}

\begin{exercice}
Réalisez ensuite la fonction \lstinline{hash_function()} en utilisant la méthode de hachage par division, et visualisez le résultat en l'intégrant au programme fourni.
\end{exercice}
Vous observerez que la densité de points est plus faible que pour la fonction aléatoire, et donc qu'il y a plus de collisions, ce qui n'est pas étonnant vu la simplicité de la méthode par division.

\begin{exercice}
Essayez d'améliorer votre fonction de hachage en utilisant une méthode plus avancée.
\end{exercice}


%Une vraie fonction de hachage est déjà fournie par la bibliothèque \lstinline{boost}. Si celle-ci est installée sur votre machine (c'est le cas pour la machine virtuelle), vous pouvez l'utiliser en ajouter la ligne suivante dans l'entête de votre fichier:
%\begin{lstlisting}
 %#include <boost/functional/hash.hpp>
%\end{lstlisting}
 %Vous pouvez alors obtenir l'empreinte d'une string comme suit:
%\begin{lstlisting}
%boost::hash<string> string_hash;
%size_t h = string_hash("Test hash");
%\end{lstlisting}
%Le type \lstinline{size_t} est en fait un entier sans signe (\lstinline{unsigned int}), donc vous pouvez le traiter comme un entier normal pour votre programme.

%\begin{exercice}
%Remplacez votre fonction de hachage par celle fournie par la bibliothèque \lstinline{boost} et observez le résultat.
%\end{exercice}

\end{otherlanguage}
\end{document}
