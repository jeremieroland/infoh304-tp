\documentclass[a4paper,11pt]{article}

\newcommand{\thetitle}{
Séance 6
}

\input{../common/preamble-TP}


\section{Fonctions et leur complexité}
Dans l'étude de la complexité, on ne s'intéresse pas aux détails des fonctions à étudier (comme le temps de calcul d'un algorithme), mais seulement à leur comportement asymptotique (i.e. à long terme, avec une taille d'entrée très grande, etc.). Les trois notations asymptotiques les plus courantes sont\footnote{On utilise ici un abus de notation très courant qui consiste à remplacer ``$\in$'' par ``$=$'' (voir slides du cours 5).}:
\begin{itemize}
\item $f(n)=O(g(n))$: Il existe $n_0> 0$ et $c> 0$ tels que $f(n)\leq c\cdot g(n)$ quand $n\geq n_0$.
\item $f(n)=\Omega(g(n))$: Il existe $n_0> 0$ et $c> 0$ tels que $f(n)\geq c\cdot g(n)$ quand $n\geq n_0$.
\item $f(n)=\Theta(g(n))$: Il existe $n_0> 0, c>0$ et $c'> 0$ tels que $c'\cdot g(n)\leq f(n)\leq c\cdot g(n)$ quand $n\geq n_0$.
\end{itemize}


\begin{exercice}
La fonction $f(n)=\frac{n^2}{2}-2\cdot n$ (voir Fig.~\ref{plot-ex1}) a une complexité $\Theta(n^2)$. Donc, $\exists n_0,c,c'>0$ tels que $\forall n\geq n_0, c'\cdot n^2\leq f(n)\leq c\cdot n^2$. Prouvez-le en donnant des valeurs explicites de $n_0$, $c$ et $c'$.
\end{exercice}

\begin{figure}[!htbp]
\begin{center}
\input{figures/plot-ex1}
\caption{$f(n)=\frac{n^2}{2}-2\cdot n$}
\label{plot-ex1}
\end{center}
\end{figure}

\begin{exercice}
Dans la table ci-dessous, choisissez la bonne classe de complexité (remplacez les $?$ par $O$, $\Omega$ ou $\Theta$) dans la deuxième colonne pour les fonctions de la première colonne. Optionnel: Justifiez vos réponses en donnant des valeurs explicites de $n_0$, $c$ et $c'$.
% \begin{table}[!htdp]

\begin{center}
\begin{tabular}{|c|c|}
\hline
$n^2+n$&$?(n^2)$\\
\hline
$\frac{n^2+n}{n+2}$&$?(n)$\\
\hline
$n+\log{(n!)}$&$?(n\cdot \log{n})$\\
\hline
$\log_{3}{(n+1)}$&$?(\log{n})$\\
\hline
$3^n$&$?(2^n)$\\
\hline
$n\cdot \sin{n}$&$?(n)$\\
\hline
\end{tabular}
\end{center}
% \caption{Choisir le bon comportement asymptotique}
% \end{table}%

\end{exercice}

\section{Application du théorème maître}
Pour analyser la performance d'un algorithme, on doit souvent résoudre une équation de récurrence. La solution d'une équation de récurrence est difficile à trouver en général. Néanmoins, pour les cas ayant un comportement simple, on a un outil mathématique puissant: \textit{le théorème maître}.

Pour rappel (voir introduction théorique), le théorème maître donne la solution d'une équation de récurrence de la forme
\begin{align}
T(n)=a\cdot T(\frac{n}{b})+f(n)\nonumber
\end{align}
pour les 3 cas suivants:
\begin{enumerate}
\item Si $f(n)=O(n^{\log_b{a}-\epsilon})$ pour une constante $\epsilon>0$, alors $T(n)=\Theta(n^{\log_b{a}})$.
\item Si $f(n)=\Theta(n^{\log_b{a}})$, alors $T(n)=\Theta(n^{\log_b{a}}\cdot\log{n})$.
\item Si $f(n)=\Omega(n^{\log_b{a}+\epsilon})$ pour une constante $\epsilon>0$, et si $a\cdot f(\frac{n}{b})\leq c\cdot f(n)$ pour une constante $c<1$ et $n$ assez grand, alors $T(n)=\Theta(f(n))$.
\end{enumerate}

Formules utiles (les deux premières sont à connaître!)
\begin{itemize}
	\item Somme d'une progression arithmétique:
	$
	\sum_{i=0}^{T-1} i=\frac{T(T-1)}{2}
	$
	\item Somme d'une progression géométrique: pour tout $x\neq 1$, 
	$
	\sum_{i=0}^{T-1} x^i=\frac{x^T-1}{x-1}
	$
	\item Pour $x<1$, on obtient par dérivation de la dernière formule (avec $T\to\infty$):
	$
		\sum_{i=0}^{\infty}i\cdot x^i=\frac{x}{(1-x)^2}
	$
\end{itemize}



\begin{exercice}
Résolvez l'équation de récurrence $T(n)=2\cdot T(\frac{n}{2})+\sqrt{n}$ avec le théorème maître. A quel cas correspond-t-elle? Représentez l'arbre de récurrence en posant $T(1)=1$ et observez quel niveau de l'arbre a la contribution la plus importante (plutôt vers la racine ou plutôt vers les feuilles?).
\end{exercice}

\begin{exercice}
Idem pour l'équation de récurrence $T(n)=4\cdot T(\frac{n}{2})+n^2+n$, avec $T(1)=2$.
\end{exercice}

\begin{exercice}
Idem pour l'équation de récurrence $T(n)=2\cdot T(\frac{n}{4})+n\cdot\log{n}$, avec $T(1)=0$.
\end{exercice}

Malheureusement, cet théorème ne couvre pas tous les cas possibles: il y a un écart entre les cas 1 et 2, et aussi entre les cas 2 et 3. Donc pour l'utiliser, il faut s'assurer que la fonction $f(n)$ se comporte relativement bien (c-à-d que l'écart entre $f(n)$ et $n^{\log_b{a}}$ est toujours d'ordre polynomial).

\begin{exercice}(Optionnel)
Pourquoi le théorème maître ne peut-il pas être utilisé pour résoudre l'équation suivante: $T(n)=2\cdot T(\frac{n}{2})+\frac{n}{\log{n}}$? Pouvez-vous la résoudre avec un arbre de récurrence? Aide: utilisez les nombres harmoniques introduits ci-dessous.
\end{exercice}

Le $n$-ième nombre harmonique $H_n$ est défini comme suit:
\begin{align}
H_n=\sum_{1\leq k\leq n} \frac{1}{n}.
\end{align}
Cette définition peut aussi s'écrire sous la forme d'une équation de récurrence:
\begin{align}
H_1&=1, & H_n&=H_{n-1}+\frac{1}{n}\qquad \forall\ n>1.
\end{align}  
Les nombres harmoniques sont notamment importants de part leur relation avec $\ln(n)$:
\begin{align}
%H_n=\ln{n}+\lim_{n\to \infty}{(H_n-\ln{n})}+\frac{1}{2\cdot n}-\frac{1}{12\cdot n^2}+\ldots,
H_n=\ln{n}+\gamma+\frac{1}{2\cdot n}-\frac{1}{12\cdot n^2}+\ldots=\ln{n}+\gamma+O\left(\frac{1}{n}\right)
\end{align}
où $\gamma\approx 0.577$ est le nombre d'Euler-Mascheroni.

\end{otherlanguage}
\end{document}