\documentclass[a4paper,11pt]{article}


\newcommand{\thetitle}{
Séance 5
}

\input{../common/preamble-TP}

Le programme Java \code{WordCount.java} disponible sur l'Université Virtuelle compte le nombre de mots uniques dans un fichier de texte. Avant de compter le nombre de mots, le texte est converti en minuscules et les ponctuations sont supprimées. Vous pouvez l'utiliser pour vérifier le résultat de votre programme C++. Il sera ausi utilisé à la fin de cette séance pour comparer les performances d'algorithmes de tri fournis par Java et C++. Ce programme Java peut être compilé et exécuté avec les commandes suivantes (où \code{sherlock.txt} est le fichier dont on veut compter les mots):
\begin{lstlisting}[language=java,mathescape=false]
javac Wordcount.java
java Wordcount sherlock.txt
\end{lstlisting}
 
\section*{Utilisation de la STL}
Durant cette séance, nous allons utiliser deux conteneurs de la bibliothèque standard du C++:
\begin{itemize}
 \item Le conteneur \code{set} réalise un type de données abstrait\footnote{Un type de données abstrait est la spécification d'un ensemble d'opérations devant être supportées par une structure de données (voir cours 6.)} nommé \textit{ensemble} (\textit{set} en anglais), qui stocke des objets sans doublon. Les principales opérations supportées sont l'insertion et la suppression d'un objet, ainsi que le test d'appartenance d'un objet dans l'ensemble.
\item Le conteneur \code{map} réalise un type de données abstrait nommé \textit{dictionnaire} (\textit{dictionary} ou \textit{map} en anglais), qui stocke des paires (clé-valeur), sans doublon pour les clés. Les principales opérations supportées sont l'insertion d'une paire (clé-valeur), la recherche, la modification et la suppression de la paire ayant une clé donnée.
\end{itemize}

La liste chaînée que vous avez implémentée la semaine passée satisfait les spécifications d'un \textit{dictionnaire}: la clé est un mot et la valeur correspond au nombre d'occurrences du mot dans le texte (notez que pour un véritable dictionnaire, la clé serait aussi un mot, alors que la valeur serait la définition).

L'utilisation d'une liste chaînée pour implémenter un \textit{dictionnaire} n'est pas la meilleure solution. Pour nous en convaincre, nous allons comparer la liste chaînée avec les conteneurs \code{set} et \code{map} de la STL. Ici, nous allons utiliser ces conteneurs pour compter les mots uniques dans <<Sherlock Holmes>>. Voici un exemple d'utilisation du conteneur \code{set} (fichier ``set\_example.cpp'' sur l'Université Virtuelle).
\lstinputlisting[]{code/set_example.cpp}


La fonction \code{insert()} de \code{set} permet d'insérer un mot dans le \code{set}. La valeur de retour de cette fonction est une paire \code{pair<iterator,bool>} (notez que \code{pair} est aussi un conteneur de la STL), où l'itérateur pointe vers le mot venant d'être inséré, la valeur booléenne est \code{true} si le mot est bien inséré et \code{false} si le mot est déjà présent dans le \code{set}.

\begin{exercice}
En utilisant la STL, comptez le nombre de mots uniques dans <<Sherlock Holmes>>, et imprimez dans la console cette liste de mots suivie de ce nombre. Aide: vous pouvez combiner l'exemple de code ci-dessus avec le code de la fonction \code{main()} que vous avez écrit lors de la dernière séance. Pour imprimer le contenu d'un conteneur, vous pouvez vous inspirer de la template \code{print} introduite au cours 4 dans le cadre du conteneur \code{vector}.
\end{exercice}

On va maintenant utiliser le conteneur \code{map} pour stocker non pas des mots mais des paires (mot, nombre d'occurrences). Le fichier ``map\_example.cpp'' disponible sur l'Université Virtuelle fournit un exemple d'utilisation du conteneur \code{map}.
Pour accéder à une valeur associée à une clé, l'opérateur \code{[]} est surchargé pour donner un comportement similaire à un tableau (dans l'exemple, \code{dictionnaire["test"]} vaut \code{10} après insertion de la paire \code{("test",10)}).
\begin{exercice}
Utilisez le conteneur \code{map} pour compter le nombre d'occurrences de chaque mot de <<Sherlock Holmes>> et écrire les paires (mot, nombre d'occurrences) dans un fichier de texte (les mots sont déjà triés à l'insertion), une paire par ligne. Aide: pour écrire les paires dans un fichier, vous pouvez également adapter la template \code{print} vue au cours 4, en tenant compte du faite que \code{operator<<} n'est pas directement défini por un objet de type \code{pair}.
\end{exercice}

Dans les systèmes Linux/Unix, pour mesurer le temps d'exécution d'un programme, on peut utiliser la commande \code{time}. Son utilisation est très simple: il suffit d'insérer le mot \code{time} avant la commande pour lancer le programme. Par exemple, la commande ci-dessous mesure le temps d'exécution du programme Java \code{WordCount}:
\begin{lstlisting}[language=bash,mathescape=false]
user@machine:tp04 $>time java WordCount war_and_peace.txt 
Total number of words in "war_and_peace.txt" : 17468
Word count file "word_count_war_and_peace.txt" created.

real	0m0.906s
user	0m1.694s
sys	    0m0.098s
user@machine:tp04 $>
\end{lstlisting}

Le résultat ci-dessus montre que la commande \code{time} donne 3 types de temps,
mais vous pouvez vous contenter de regarder la ligne \code{real} qui correspond au ``vrai'' temps, comme mesuré par une montre.
% \begin{itemize}
% \item \textit{real} signifie le ``vrai'' temps, comme mesuré par une montre.
% \item \textit{user} dénote le temps que le processus passe dans l'espace utilisateur (\textit{user space} en anglais) du système d'exploitation. L'espace utilisateur contient les fonctions de systèmes sans privilège spécial.
% \item \textit{sys} dénote le temps que le processus passe dans l'espace noyau (\textit{kernel space} en anglais) du système d'exploitation. L'espace noyau contient les fonctions restreintes comme l'accès à la mémoire, les pilotes et les appels d'interruptions. 
% \end{itemize}
% Sur une machine multi-coeur, les deux derniers types de temps sont mesurés sur tous les coeurs. Du coup il est possible que les deux derniers types de temps soient plus long que le temps ``réel''.

\begin{exercice}
Comparez les temps d'exécution de trois programmes: la liste chaînée réalisée à la séance précédente, le conteneur \code{map} de la STL et le programme Java. Lequel est le plus rapide? Quelles sont les causes possible pour la bonne performance? Vous pouvez utiliser soit le temps \code{real} soit le temps \code{user}. (Pour les deux derniers programmes, il est possible de comparer leur performance avec <<Guerre et paix>>.)
\end{exercice}

\end{otherlanguage}
\end{document}