# INFOH304-TP
Ce dépôt reprend toutes les sources LaTeX et codes de base pour les TPs du cours INFOH304. Les fichiers LaTeX pour les énoncés et les slides peuvent être compilés manuellement ou via le fichier Makefile fourni. La compilation nécessite une version récente de TeX Live (testé avec la version 2021). En cas de souci de compilation, vérifiez les erreurs pour voir s'il ne vous manque pas un paquet annexe, pour les détails voir ci-dessous.
## Compilation avec Makefile
Instructions pour chaque cible:
### `make` ou `make all`
Compile les énoncés et les slides d'introduction (le cas échéant) pour toutes les séances et place les fichiers PDF produits, ainsi que les codes de base, dans les dossiers TPXX
### `make TPXX`
Idem que `make all` mais uniquement pour la séance XX (où XX est compris entre 01 et 11)
### `make clean`
Supprime la plupart des fichiers auxiliaires créés par LaTeX lors de la compilation, sauf les images temporaires. Après un `make clean`, un appel à `make` ne relancera pas de compilation sauf si un des fichiers sources a été modifié
### `make cleaner`
Supprime tous les fichiers auxiliaires créés par LaTeX lors de la compilation, y compris les images temporaires. Après un `make cleaner`, un appel à `make` relancera la compilation de tous les fichiers (ce qui peut être long en raison des nombreuses images à générer)
## Compilation manuelle
### Enoncés
Pour compiler les fichiers à la main, utiliser la commande suivante (notez l'utilisation de lualatex et les options de compilation):
> lualatex -synctex=1 -interaction=nonstopmode enonces.tex

Pour la compilation, cette commande nécessite la présence des fichiers `preamble-TP.tex` et `obfuscate.lua` dans le dossier supérieur, ainsi qu'une copie du fichier `Inconsolata.otf` dans le dossier courant (pour éviter de multiples copies de ce fichier, un lien symbolique peut être créé à la place au moyen de la commande `ln -s ../Inconsolata.otf` dans le dossier courant)
### Slides d'introduction
Pour compiler les slides d'introduction à la main, utiliser la commande suivante (notez les options de compilation):
> pdflatex -synctex=1 -interaction=nonstopmode -shell-escape slides.tex

Pour la génération des images temporaires, cette commande nécessite l'existence d'un sous-dossier `figures/externalize`, et la présence du fichier `preamble.tex` dans le dossier supérieur
