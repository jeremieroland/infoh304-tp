PREAMBLEFILESTP = sources/common/preamble-TP.tex sources/common/obfuscate.lua
PREAMBLEFILESSLIDES = sources/common/preamble.tex
SOURCES = $(wildcard sources/TP??)
TARGETS = $(patsubst sources/%,%,$(SOURCES))
PRECIOUS = $(addsuffix /enonces.pdf,$(SOURCES)) $(addsuffix /slides.pdf,$(SOURCES)) $(addsuffix /slides-animated.pdf,$(SOURCES)) $(addsuffix /figures/externalize,$(SOURCES)) $(addsuffix /Inconsolata.otf,$(SOURCES))

.PRECIOUS: $(PRECIOUS)
# .IGNORE: sources/TP*/slides.pdf
all: $(TARGETS)
clean:
	for directory in $(SOURCES); do cd $${directory} &&latexmk -c -r ../common/latexmkrc && latexmk -f -c -r ../common/latexmkrc slides-animated.tex && cd ../.. ; done
cleaner:
	for directory in $(SOURCES); do cd $${directory} &&latexmk -C -r ../common/latexmkrc && latexmk -f -C -r ../common/latexmkrc slides-animated.tex && rm -rf figures/externalize && rm -rf Inconsolata.otf && cd ../.. ; done
.SECONDEXPANSION:
TP%: sources/TP%/enonces.pdf sources/TP%/README.md $$(subst .tex,.pdf,$$(wildcard sources/TP%/slides.tex))
	rm -rf TP$*
	mkdir TP$*
	cp sources/TP$*/enonces.pdf TP$*/TP$*-enonces.pdf
	cp sources/TP$*/README.md TP$*/README.md
	if [ -f sources/TP$*/slides.pdf ]; then \
	cp sources/TP$*/slides.pdf TP$*/TP$*-slides.pdf; fi
	if [ -d sources/TP$*/code ]; then \
	cp -R sources/TP$*/code TP$*/code; fi
TP%-animated: $$(subst .tex,-animated.pdf,$$(wildcard sources/TP%/slides.tex))
	mkdir -p TP$*
	if [ -f sources/TP$*/slides-animated.pdf ]; then \
		cp sources/TP$*/slides-animated.pdf TP$*/TP$*-slides-animated.pdf; fi
sources/TP%/enonces.pdf: sources/TP%/enonces.tex sources/TP%/Inconsolata.otf $(PREAMBLEFILESTP) $$(wildcard sources/TP%/code/*) $$(wildcard sources/TP%/code-enonces/*)
	latexmk -pdflua -cd -synctex=1 -interaction=nonstopmode -silent $<
sources/TP%/slides.pdf: sources/TP%/slides.tex $(PREAMBLEFILESSLIDES) $$(wildcard sources/TP%/code/*) $$(wildcard sources/TP%/code-slides/*) $$(wildcard sources/TP%/figures/*) sources/TP%/figures/externalize
	latexmk -pdf -cd -synctex=1 -interaction=nonstopmode -shell-escape -silent $<
sources/TP%/figures/externalize:
	mkdir -p sources/TP$*/figures/externalize
sources/TP%/Inconsolata.otf:
	ln -s ../common/Inconsolata.otf ./sources/TP$*/.
animated: $(addsuffix -animated,$(TARGETS))
sources/TP%/slides-animated.pdf: sources/TP%/slides.tex $(PREAMBLEFILESSLIDES) $$(wildcard sources/TP%/code/*) $$(wildcard sources/TP%/code-slides/*) $$(wildcard sources/TP%/figures/*) sources/TP%/figures/externalize
	echo "\def\Animated{}" > sources/TP$*/version.tex
	ln -s slides.tex sources/TP$*/slides-animated.tex
	latexmk -pdf -cd -synctex=1 -interaction=nonstopmode -shell-escape -jobname="slides-animated" -silent $<
	rm -rf sources/TP$*/version.tex
	rm -rf sources/TP$*/slides-animated.tex
